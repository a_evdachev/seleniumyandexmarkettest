package tests;

import helpers.ActionOnPage;
import helpers.pages.Market;
import helpers.pages.SearchPrepack;
import helpers.pages.Yandex;
import io.qameta.allure.Epic;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
//import io.qameta.allure.junit4.DisplayName;
import org.junit.*;

@Epic("Тест Market.Yandex")
public class TestSearchTablet {
    private static ActionOnPage action;

    @BeforeClass
    public static void oneTimeSetUp(){
        action = new ActionOnPage();
    }

    @Test
    //@DisplayName("Тест выдачи планшета фирмы ASUS в определенной ценовой категории и проверка поиска полученного планшета в Market.Yandex.ru")
    @Severity(SeverityLevel.MINOR)
    public void testFindingTabletCertainPriceCategory() {
        // Атрибуты теста
        String url = "https://www.yandex.ru/";
        String name_firm = "ASUS";
        String price_from = "20000";
        String price_to = "25000";


        // Переход на экран Yandex.ru
        action.navigateToPage(url);
        // Переход на экран "Маркет"
        action.clickElementXPath(Yandex.LinkMarket_xpath);
        // Переход на экран "Планшеты"
        action.clickElementXPath(Market.TopMenuLinkComputers_xpath);
        action.clickElementXPath(Market.TopMenuLinkPlate_xpath);
        // Переход на экран "Фильтров"
        settingPriceRangeTabletCompany(price_from, price_to, name_firm);
        // Запоминание название второго планшета из результата выдачи
        String remember_tho_name_plate = action.getTextElement(Market.TwoElementResult_xpath);
        // Выполнение поиска планшета по его названию
        productSearchModelName(remember_tho_name_plate);
        String result_name_plate = action.getTextElement(Market.ResultH1Name_xpath);


        // Проверка результата теста
        Assert.assertEquals(remember_tho_name_plate , result_name_plate);
    }

    @AfterClass
    public static void oneTimeTearDown(){
        // Освобождения ресурса
        action.getDriver().close();
        action.getDriver().quit();
        action = null;
    }

    /**
     * private methods
     */

    /*
      Настройка и применение фильтра пл ценовому диапазону поиска и названию фирмы
     */
    private void settingPriceRangeTabletCompany(String price_from , String price_to, String name_firm){
        action.clickElementXPath(Market.SearchPrepack_xpath);
        action.clickElementXPath(SearchPrepack.ButtonShowAll_Manufacturer_xpath);

        // Ожидание отображения элемента поиска
        action.WaitElement(action.findElement_xpath(SearchPrepack.InputSearch_Manufacturer_xpath));

        // Поиск фирмы по name_firm
        action.sendKeysElementXPath(SearchPrepack.InputSearch_Manufacturer_xpath, name_firm);

        action.clickElementXPath(SearchPrepack.CheckBocs_ASUS_xpath);

        // Задание диапазона цен
        action.sendKeysElementXPath(SearchPrepack.InputPricefrom_xpath,price_from);
        action.WaitValueInUrl(price_from); // Ожидание заполнения url введенными данными
        action.sendKeysElementXPath(SearchPrepack.InputPriceto_xpath,price_to);
        action.WaitValueInUrl(price_to);

        // Применение фильтра
        action.clickElementXPath(SearchPrepack.ButtonShowAppropriate_xpath);
    }

    /*
      Выполнение поиска планшета в Market.Yandex.ru по его названию
     */
    private void productSearchModelName(String name_plate){
        action.sendKeysElementXPath(Market.HeaderSearch_xpath, name_plate);
        action.clickElementXPath(Market.ButtonHeaderSearch_xpath);
    }
}