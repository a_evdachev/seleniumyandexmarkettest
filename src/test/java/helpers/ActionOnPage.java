package helpers;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;

public class ActionOnPage {
    private WebDriver driver;
    private Wait<WebDriver> driverWait;

    public WebDriver getDriver() {
        return driver;
    }

    public ActionOnPage(){
        SettingWebDriver settingWebDriver = new SettingWebDriver();
        driver = settingWebDriver.getWebDriver();
        driverWait = settingWebDriver.getWebDriverWait();
    }

    public void navigateToPage(String url){
        if (url == null) return;
        driver.navigate().to(url);
    }

    public void WaitElement(WebElement element){
        driverWait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void WaitValueInUrl(String value){
        driverWait.until(ExpectedConditions.urlContains(value));
    }

    public void clickElementXPath(String xpath){
        findElement_xpath(xpath).click();
    }

    public void sendKeysElementXPath(String xpath, String text){
        findElement_xpath(xpath).sendKeys(text);
    }

    public String getTextElement(String xpath){
        return findElement_xpath(xpath).getText();
    }

    public WebElement findElement_xpath(String xpath){
        if(xpath == null) return null;

        return driver.findElement(By.xpath(xpath));
    }
}