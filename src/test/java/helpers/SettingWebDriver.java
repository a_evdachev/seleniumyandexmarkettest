package helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class SettingWebDriver {

    private WebDriver webDriver = null;
    private Wait<WebDriver> webDriverWait = null;

    public WebDriver getWebDriver() {
        if (webDriver == null){
            webDriver = createWebDriver();
            return webDriver;
        }else {
            return webDriver;
        }
    }

    public Wait<WebDriver> getWebDriverWait() {
        if (webDriverWait == null){
            webDriverWait = createWebDriverWait();
            return webDriverWait;
        } else {
            return webDriverWait;
        }
    }

    /*
    private methods
     */

    private WebDriver createWebDriver(){
        String OS = System.getProperty("os.name");
        WebDriver driver;

        if(OS.equals("Mac OS X")) {
            driver = new ChromeDriver();
        } else {
            driver = new InternetExplorerDriver();
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().fullscreen();

        return driver;
    }

    private Wait<WebDriver> createWebDriverWait(){
        if (webDriver == null) getWebDriver();

        return new WebDriverWait(webDriver, 5, 1000);
    }
}
