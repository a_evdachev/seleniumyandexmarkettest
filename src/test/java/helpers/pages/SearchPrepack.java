package helpers.pages;

public class SearchPrepack {
    public final static String ButtonShowAll_Manufacturer_xpath = "//button[@type='button']";

    public final static String InputSearch_Manufacturer_xpath = "//div[2]/div[2]/div/span/span/input";

    public final static String CheckBocs_ASUS_xpath = "//label[contains(text(), 'ASUS')]";

    public final static String InputPricefrom_xpath = "//*[@id=\"glf-pricefrom-var\"]";

    public final static String InputPriceto_xpath = "//*[@id=\"glf-priceto-var\"]";

    public final static String ButtonShowAppropriate_xpath = "//div[1]/div[5]/a[2]";
}