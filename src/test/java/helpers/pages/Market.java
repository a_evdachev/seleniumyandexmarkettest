package helpers.pages;

public class Market {
    public final static String TopMenuLinkComputers_xpath = "//noindex/ul/li[2]/a";

    public final static String TopMenuLinkPlate_xpath = "//div/a[1][contains(text(),'Планшеты')]";

    public final static String SearchPrepack_xpath = "//*[@id=\"search-prepack\"]//a[contains(text(),'Перейти ко всем фильтрам')]";

    public final static String TwoElementResult_xpath = "//div[2]/div[4]/div[1]/div[1]/a";

    public final static String HeaderSearch_xpath = "//*[@id='header-search']";

    public final static String ResultH1Name_xpath = "//h1";

    public final static String ButtonHeaderSearch_xpath = "//button[@type='submit']";
}
